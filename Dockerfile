FROM python:alpine

RUN apk update && apk add git

RUN mkdir /docs
WORKDIR /docs

COPY requirements.txt /docs/
RUN pip install -r requirements.txt

CMD [ "mkdocs", "serve", "--dev-addr=0.0.0.0:8000" ]