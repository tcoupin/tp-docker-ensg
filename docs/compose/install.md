La procédure est fournie dans la documentation : [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/).

Pour résumer :

```
$ sudo apt-get update
$ sudo apt-get install docker-compose-plugin
```


Pour vérifier l'installation :
```
docker compose version
```
