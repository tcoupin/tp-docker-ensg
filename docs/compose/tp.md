# TP1 : LAMP

Reproduire [le TP LAMP](../../engine/tp-lamp/) dans un fichier docker-compose.yml.
{% if config.extra.show_correction.compose.tp %}
??? note "Correction"

    Voici le fichier `docker-compose.yml` final :

    ```yaml
    version: "3"

    services:
      web:
        image: lavoweb/php-7.1
        volumes:
          - ./mondossier:/var/www/html
        ports:
          - "8080:80"

      database:
        image: mariadb
        environment:
          - MARIADB_RANDOM_ROOT_PASSWORD=yes
          - MARIADB_DATABASE=mymap
          - MARIADB_USER=user
          - MARIADB_PASSWORD=s3cr3t
        volumes:
          - databasedata:/var/lib/mysql

    volumes:
      databasedata:
    ```

    On lance les conteneurs avec la commande 

    ```
    docker compose up -d
    ```

    Contrairement au TP en ligne de commande, il n'est pas nécessaire de créer un nouveau réseau pour bénéficier de la fonctionnalité DNS. Compose le créé d'office si on ne spécifie pas d'option réseau dans le fichier yaml.

    On peut arrêter et supprimer les conteneurs avec la commande :
    
    ```
    docker compose down -v
    ```
{% endif %}

# TP2 : avec construction d'image

Reproduire le [TP dockerfiles partie LibreQR ou Geoserver ou GRR,](../../engine/tp-dockerfile/) avec docker compose en utilisant la fonctionnalité de construction d'image.

*Bonus pour la version geoserver : vous pouvez ajouter un conteneur [postgis](https://hub.docker.com/r/camptocamp/postgres), configurez geoserver pour l'utiliser et utiliser QGIS pour interagir avec postgis (pour la création des données) et avec geoserver (pour l'affichage).*

# TP3 : scaling

Reprendre le TP1 et utiliser la commande `docker-compose scale ...` pour multiplier le nombre de conteneur web. Que se passe-t-il ?

## Récréation : le load-balancing en pratique

[Voir ici...](../../recreation/load-balancing/)

## Le loadbalancing en pratique

Modifiez votre docker-compose.yml pour permettre le scalling du conteneur web.

**Ici nous scalons uniquement la partie web. Le scaling d'une base de données est plus complexes car il faut que la base de données ait des concepts de clustering applicatif pour le partage des données.**
