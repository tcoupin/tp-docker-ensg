Nous allons utiliser Docker dans un environnement Linux avec la distribution Ubuntu. 

> Le projet [PWD](http://play-with-docker.com) permet également de tester Docker sans l'installer au travers d'une interface web. Les sessions de travail durent 4h et la vitesse de l'interface dépend souvent de l'activité des autres utilisateurs...

## Variante

Docker Engine est disponible en 2 variantes :

- CE : *community edition* qui est la version gratuite que nous allons utiliser ;
- EE : *entreprise edition* qui est plus évoluée avec des fonctionnalités supplémentaires et une certification de fonctionnement sur certain matériel.

## Installation

La procédure est fournie dans la documentation : [https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/).

Pour résumer :

```bash
curl https://get.docker.com/ | sudo sh
sudo usermod -a -G docker $USER
```

??? info "Utilisation d'un proxy"
    Si vous devez utiliser un proxy pour l'accès à internet, lancer ces commandes avant la commande curl :
    ```
    export HTTP_PROXY=http://PROXY_IP:PROXY_PORT
    export HTTPS_PROXY=http://PROXY_IP:PROXY_PORT
    ```

Pour que la dernière commande soit prise en compte, il faut fermer la session et la rouvrir.


??? info "Utilisation d'un proxy"
    Si vous devez utiliser un proxy pour l'accès à internet, suivre ces étapes :

    1. Localiser le fichier de service avec `sudo systemctl status docker` : `/lib/systemd/system/docker.service`

    2. Dans la section `[service]`, ajouter :

        ```
        Environment="HTTP_PROXY=http://PROXY_IP:PROXY_PORT"
        Environment="HTTPS_PROXY=http://PROXY_IP:PROXY_PORT"
        ```

    3. Redémarrer le daemon docker : `sudo systemctl restart docker`

Pour vérifier l'installation :
```
docker run hello-world
```