Le but est de mettre en place NextCloud, un gestionnaire de fichier en ligne.
L'image à utiliser est nextcloud, disponible sur [https://hub.docker.com/_/nextcloud/](https://hub.docker.com/_/nextcloud/). Cette page contient beaucoup de détails que vous pouvez trouver dans les métadonnées de l'image.

## Etape 1 : un serveur simple avec une base de données SQLite (un fichier)

- Lancez un conteneur exposant sur le port 8080 de la machine hôte le serveur nextcloud et rendez-vous sur [http://127.0.0.1:8080](http://127.0.0.1:8080) pour accéder à l'interface d'initialisation du serveur. Suivre les étapes d'installation.

{% if config.extra.show_correction.engine.tpdeploy %}
    ??? note "Correction"

        ```shell
        docker run -d --name nextcloud -p 8080:80 nextcloud
        ```

        Une fois sur http://127.0.0.1:8080, choisir un nom d'utilisateur pour l'administrateur et un mot de passe. En laissant les informations par défaut, nextcloud utilise :
        
        - une base SQLite pour stocker ses infos. Il s'agit d'une base de données sous forme d'un fichier.
        - l'emplacement `/var/www/html/data` pour stocker ce fichier sqlite et les fichiers qui seront transférés par les utilisateurs.

        Une fois l'installation terminée, on test l'application. Dans le menu en haut, choisir fichier puis cliquer sur le bouton "+" pour envoyer un fichier.

        On peut retrouver ce fichier avec les commandes 

        ```
        docker container exec nextcloud ls -al /var/www/html/data/NOM_UTILISATEUR/files/
        ```
{% endif %}

- Ajoutez des volumes pour que les données soient préservées lorsque le conteneur est détruit (et vérifiez que c'est bien le cas...)

{% if config.extra.show_correction.engine.tpdeploy %}
    ??? note "Correction"

        ```shell
        docker run -d --name nextcloud -p 8080:80 -v nextcloud-data:/var/www/html/data/ nextcloud
        ```
{% endif %}

## Etape 2 : utiliser une base de données externe MariaDB ou PostgreSQL

- Lancez un conteneur MariaDB ou PostgreSQL (n'oubliez pas les volumes...)

{% if config.extra.show_correction.engine.tpdeploy %}
    ??? note "Correction"

        Voir le TP1 pour la mise en place d'un conteneur MariaDB, avec un réseau dédié et un volume bien placé.
{% endif %}

- Recréez le conteneur nextcloud avec les bonnes options pour qu'à la configuration, vous puissiez lui dire d'utiliser la base de donnée mise en place.
