
## Un dockerfile pour LibreQR

LibreQR est une application web de génération de QR Code. Le code source et la documentation se trouvent là : [https://code.antopie.org/miraty/libreqr](https://code.antopie.org/miraty/libreqr).

Ecrire un dockerfile basé sur l'image `php:apache`. Pour installer la dépendance `php-gd` on utilisera plutôt https://github.com/mlocati/docker-php-extension-installer.

Penser à rendre le dossier et les fichiers accessibles à l'utilisateur du service httpd `www-data`.

Utiliser la configuration PHP de production et non de developpement (voir la partie "Configuration" de https://hub.docker.com/_/php)

Modifier le fichier de configuration pour changer le texte afficher en bas de page. Ce fichier doit être inclut dans l'image construite.

{% if config.extra.show_correction.engine.tpdockerfile %}
??? note "Correction"

    !!! abstract "Dockerfile"
        ```
        FROM php:apache
        
        ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
        
        RUN chmod +x /usr/local/bin/install-php-extensions && \
            install-php-extensions gd
        
        ADD https://code.antopie.org/miraty/libreqr/archive/main.zip /main.zip
        
        RUN apt-get update && \
            apt-get install -y unzip && \
            cd /var/www/html && \
            unzip /main.zip && \
            mv libreqr/* . && \
            chown -R www-data:www-data /var/www/html/
        
        ADD config.inc.php /var/www/html
        RUN chown -R www-data:www-data /var/www/html/
        ENV LIBREQR_THEME=libreqr \
            LIBREQR_DEFAULT_LOCALE=fr \
            LIBREQR_CUSTOM_TEXT_ENABLED=true \
            LIBREQR_CUSTOM_TEXT="LibreQR on docker" \
            LIBREQR_DEFAULT_REDUNDANCY=high \
            LIBREQR_DEFAULT_MARGIN=20 \
            LIBREQR_DEFAULT_SIZE=300 \
            LIBREQR_DEFAULT_BGCOLOR=FFFFFF \
            LIBREQR_DEFAULT_FGCOLOR=000000
        ```

    !!! abstract "config.inc.php"
        ```
        <?php
        // Basé sur https://code.antopie.org/miraty/libreqr/src/branch/main/config.inc.php
        // This file is part of LibreQR, which is distributed under the GNU AGPLv3+ license

        // LIBREQR SETTINGS

        // Theme's directory name
        define("THEME", $_ENV["LIBREQR_THEME"]);

        // Language used if those requested by the user are not available
        define("DEFAULT_LOCALE", $_ENV["LIBREQR_DEFAULT_LOCALE"]);

        // Will be printed at the bottom of the interface
        define("CUSTOM_TEXT_ENABLED", $_ENV["LIBREQR_CUSTOM_TEXT_ENABLED"] == "true");
        define("CUSTOM_TEXT", $_ENV["LIBREQR_CUSTOM_TEXT"]);

        // Default values
        define("DEFAULT_REDUNDANCY", $_ENV["LIBREQR_DEFAULT_REDUNDANCY"]);
        define("DEFAULT_MARGIN", intval($_ENV["LIBREQR_DEFAULT_MARGIN"]));
        define("DEFAULT_SIZE", intval($_ENV["LIBREQR_DEFAULT_SIZE"]));
        define("DEFAULT_BGCOLOR", $_ENV["LIBREQR_DEFAULT_BGCOLOR"]);
        define("DEFAULT_FGCOLOR", $_ENV["LIBREQR_DEFAULT_FGCOLOR"]);

        ```

    ```
    docker image build -t qr .
    docker container run -p 8080:80 -e LIBREQR_CUSTOM_TEXT="Générateur de QR code dans docker" qr
    ```
{% endif %}

## Un dockerfile pour Geoserver

Dans ce TP, nous allons créer une image geoserver. Voici la documentation d'installation [https://docs.geoserver.org/latest/en/user/installation/war.html](https://docs.geoserver.org/latest/en/user/installation/war.html).

Geoserver est une application web écrite en java. Elle se déploit sur un serveur `tomcat` (dans le dossier webapps).
Geoserver utilise un dossier `workspace` comme espace de travail. Elle y stocke sa configuration et ses données.

Voici également quelques instruction pour l'écriture du Dockerfile :

- partir de l'image `tomcat`
- la commande `ADD` permet d'ajouter des fichiers depuis internet vers l'image
- déclarer les volumes où sont stockées les données


## Un dockerfile pour GRR

GRR est une application php de gestion de ressources partagées (planning de réservation de salle de réunion etc...).

Ecrire un dockerfile basé sur une image php permettant de lancer l'application GRR. La base de donnée est bien sûr en dehors de ce conteneur.

## Multi-stage build

> *Jekyll* permet de créer un site web statique à partir de fichier texte écrits en markdown. Il est écrit en ruby et est utilisé notamment par github.


### Prise en main de Jekyll

Docs utiles :

- [Commandes utiles de Jekyll](https://jekyllrb.com/docs/usage/)
- [Thème *minima* pour Jekyll](https://github.com/jekyll/minima)
- Image officielle : `jekyll/jekyll`

Ecrire un Dockerfile se basant sur l'image officielle de Jekyll, qui créé un nouveau projet : `jekyll new monsite`.
La commande par défaut de cette image doit être `jekyll serve` qui construit les fichiers html à partir des markdowns et démarre le serveur web inclus dans Jekyll. Le port 4000 sera exposé sur le port 80 de la machine hôte.

### Un serveur web avec juste le site

Dans cette configuration, notre image inclut les dépendances de construction : jekyll. Nous allons utiliser le concept des *multi-stage builds* pour créer une image contenant uniquement un serveur web et les fichiers HTML, JS et CSS de notre site web sans Jekyll et ses dépendances. Cette fois-ci on utilisera la commande `jekyll build` pour uniquement construire le site web. On copiera les fichiers générés dans l'image du serveur web. Le serveur web à utiliser est l'image `httpd:alpine`
