*Le but de ce TP est de mettre en place les éléments nécessaires d'un serveur web de type LAMP.*

> LAMP :
>
> - **L**inux
> - **A**pache
> - **M**ySql
> - **P**HP

## Apache

L'image à utiliser ici est `httpd` :

1. Lancez un conteneur exposant le port `80` du conteneur sur le port `8080` de la machine. Qu'affiche la page `http://127.0.0.1:8080` ?

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"
        ```
        docker container run -d --name web -p 8080:80 httpd
        ```

        Le navigateur affiche la page "It's work".
        
        - L'option `-d` permet de lancer le conteneur en arrière plan. On peut ainsi faire d'autre commande sans avoir à recréer un nouveau terminal.
        - L'option `--name` permet de choisir le nom au conteneur pour pouvoir le manipuler sans avoir à trouver son nom avant chaque commande.
{% endif %}

2. Cette image utilise le contenu du dossier `/usr/local/apache2/htdocs/` pour répondre aux requêtes. Relancez un nouveau conteneur en utilisant un volume hôte (un dossier de la machine).

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"
        ```shell
        docker container stop web
        docker container rm web
        mkdir mondossier
        docker container run -d --name web -p 8080:80 -v $PWD/mondossier:/usr/local/apache2/htdocs/ httpd
        ```

        La variable $PWD contient l'emplacement courant du terminal.

        Le navigateur affiche "Index of /" et rien d'autre, signe que le dossier de travail de httpd est vide
{% endif %}

3. Dans ce dossier placez le fichier index.html et observez le résultat :

    ```html
    <body style="background-color: yellow;">
    <h1 style="font-family: sans;padding-top: 20vh;text-align: center;font-size: 5em;">
    Salut
    </h1>
    </body>
    ```

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"

        Pour simplifier la correction, le changement du contenu des fichiers est fait avec la syntaxe 
        ```
        cat << 'EOF' > nomdufichier
        CONTENU
        EOF
        ```

        En pratique, on peut utiliser l'éditeur de texte habituel : vim, nano, Visual Studio Code, Gedit, Geany...

        ```
        cat << 'EOF' > mondossier/index.html
        <body style="background-color: yellow;">
        <h1 style="font-family: sans;padding-top: 20vh;text-align: center;font-size: 5em;">
        Salut
        </h1>
        </body>
        EOF
        ```

        Maintenant, le navigateur affiche une page jaune avec le texte "Salut".
{% endif %}
## Un peu de php

1. Remplacez le fichier html par `index.php`

    ```html
    <body style="background-color: green;">
    <h1 style="font-family: sans;padding-top: 20vh;text-align: center;font-size: 5em;">
    <?php
    echo "Salut, voici l'heure : " . date("H:i:s");
    ?>
    </h1>
    </body>
    ```

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"

        ```
        rm -f mondossier/index.html
        cat << 'EOF' > mondossier/index.php
        <body style="background-color: green;">
        <h1 style="font-family: sans;padding-top: 20vh;text-align: center;font-size: 5em;">
        <?php
        echo "Salut, voici l'heure : " . date("H:i:s");
        ?>
        </h1>
        </body>
        EOF
        ```
{% endif %}

2. ça ne fonctionne pas... Pourquoi ?

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"
        `httpd` est de base un simple serveur web sans fonctionnalité php. Il faudrait ajouter php dans cette image et configurer httpd pour interpréter les fichiers php. Sans cela, httpd cherche uniquement les fichiers `index.html` si aucun fichier n'est précisé dans l'URL. 

        Même en ajoutant `index.php` à l'URL, le résultat n'est pas satisfaisant, il n'y a qu'une page verte alors qu'elle devrait afficher l'heure.
{% endif %}

3. Utilisez plutôt l'image `lavoweb/php-7.1`. Ca ne fonctionne toujours pas... utilisez les logs pour comprendre et corriger (`docker container logs...`).

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"
        ```shell
        docker container stop web
        docker container rm web
        docker container run -d --name web -p 8080:80 -v $PWD/mondossier:/usr/local/apache2/htdocs/ lavoweb/php-7.1
        ```

        Le navigateur indique `Not Found`. Dans les logs on trouve la ligne `script '/var/www/html/index.php' not found or unable to stat,`. `httpd` cherche les fichiers dans le dossier `/var/www/html` du conteneur et non `/usr/local/apache2/htdocs/`.

        Même si cette image utilise également httpd (muni de php), sa configuration par défaut n'est pas là même que l'image précedent. Elle est configurée pour utiliser l'emplacement `/var/www/html`. Ces informations

        En modifiant le conteneur avec le bon emplacement, le navigateur affiche bien la page verte avec l'heure.

        ```shell
        docker container stop web
        docker container rm web
        docker container run -d --name web -p 8080:80 -v $PWD/mondossier:/var/www/html/ lavoweb/php-7.1
        ```
{% endif %}


## Avec une base de données

Notre site web évolue ! Il va maintenant afficher une carte. Un clic permet de créer un point, sauvegardé en base de données. Un clic sur un point le supprime. Au chargement de la page, on affiche tous les points de la base de données.

1. Remplacez le contenu du fichier `index.php` par :

    ??? abstract "index.php"

        ```html
        <?php

        $bdd = new PDO('mysql:host=database;dbname=mymap;charset=utf8', 'user', 's3cr3t');
        $bdd->exec("CREATE TABLE IF NOT EXISTS point (id INT KEY AUTO_INCREMENT, lon FLOAT, lat FLOAT);");

        switch ($_GET['action']) {
            case 'add':
                if (isset($_GET['lon']) && isset($_GET['lat'])){
                    $bdd->exec(sprintf("INSERT INTO point (lon, lat) values (%F , %F)", $_GET['lon'], $_GET['lat']));
                }
                exit;
            case 'delete':
                if (isset($_GET['id'])){
                    $bdd->exec(sprintf("DELETE FROM point where id = '%d'", $_GET['id']));
                }
                exit;
            case 'get':
                $result = $bdd->query("select * from point;");
                $features = array();
                while ($donnees = $result->fetch()){
                    $features[] = array(
                        'type' => 'Feature',
                        'properties' => array('id' => intval($donnees['id'])),
                        'geometry' => array(
                            'type' => 'Point',
                            'coordinates' => array(floatval($donnees['lon']), floatval($donnees['lat']))
                        )
                        );
                }
                $new_data = array(
                    'type' => "FeatureCollection",
                    'features' => $features,
                  );
                header("Content-type: application/json");
                echo json_encode($new_data, JSON_PRETTY_PRINT);
                exit;
        }
        ?>

        <!DOCTYPE html>
        <html>
        <head>

            <title>MyMap</title>

            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

            <link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
            <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

            <style>
                html, body, #mapid {
                    margin: 0;
                    padding: 0;
                    width: 100%;
                    height: 100%;
                }
            </style>
        </head>
        <body>
        <div id="mapid"></div>
        <script>
        
            var mymap = L.map('mapid').setView([46, 0], 6);
            var datalayer;
            var needzoom = true;

            L.tileLayer('http://basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
                maxZoom: 21,
                attribution: "<?php echo 'Page générée à ' . date("H:i:s");?>"
            }).addTo(mymap);

            function getData(){
                if (datalayer){
                    datalayer.remove()
                }
                $.get('/?action=get',function(data){
                datalayer = L.geoJson(data,{
                    onEachFeature: function(feat, layer){
                        layer.on('click',function(e){
                            $.get('/?action=delete&id='+feat.properties.id,()=>getData());
                        })
                    }
                }).addTo(mymap)
                if (needzoom){
                    needzoom = false;
                    mymap.fitBounds(datalayer.getBounds())
                }
                })
            }

            mymap.on('click', function(e){
                $.get('/?action=add&lon='+e.latlng.lng+'&lat='+e.latlng.lat,()=>getData());
            })

            getData()
        </script>
        </body>
        </html>
        ```

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"

        ```shell
        cat << 'EOF' > mondossier/index.php
        <?php

        $bdd = new PDO('mysql:host=database;dbname=mymap;charset=utf8', 'user', 's3cr3t');
        $bdd->exec("CREATE TABLE IF NOT EXISTS point (id INT KEY AUTO_INCREMENT, lon FLOAT, lat FLOAT);");

        switch ($_GET['action']) {
            case 'add':
                if (isset($_GET['lon']) && isset($_GET['lat'])){
                    $bdd->exec(sprintf("INSERT INTO point (lon, lat) values (%F , %F)", $_GET['lon'], $_GET['lat']));
                }
                exit;
            case 'delete':
                if (isset($_GET['id'])){
                    $bdd->exec(sprintf("DELETE FROM point where id = '%d'", $_GET['id']));
                }
                exit;
            case 'get':
                $result = $bdd->query("select * from point;");
                $features = array();
                while ($donnees = $result->fetch()){
                    $features[] = array(
                        'type' => 'Feature',
                        'properties' => array('id' => intval($donnees['id'])),
                        'geometry' => array(
                            'type' => 'Point',
                            'coordinates' => array(floatval($donnees['lon']), floatval($donnees['lat']))
                        )
                        );
                }
                $new_data = array(
                    'type' => "FeatureCollection",
                    'features' => $features,
                  );
                header("Content-type: application/json");
                echo json_encode($new_data, JSON_PRETTY_PRINT);
                exit;
        }
        ?>

        <!DOCTYPE html>
        <html>
        <head>

            <title>MyMap</title>

            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

            <link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" />

            <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
            <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

            <style>
                html, body, #mapid {
                    margin: 0;
                    padding: 0;
                    width: 100%;
                    height: 100%;
                }
            </style>
        </head>
        <body>
        <div id="mapid"></div>
        <script>
        
            var mymap = L.map('mapid').setView([46, 0], 6);
            var datalayer;
            var needzoom = true;

            L.tileLayer('http://basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
                maxZoom: 21,
                attribution: "<?php echo 'Page générée à ' . date("H:i:s");?>"
            }).addTo(mymap);

            function getData(){
                if (datalayer){
                    datalayer.remove()
                }
                $.get('/?action=get',function(data){
                datalayer = L.geoJson(data,{
                    onEachFeature: function(feat, layer){
                        layer.on('click',function(e){
                            $.get('/?action=delete&id='+feat.properties.id,()=>getData());
                        })
                    }
                }).addTo(mymap)
                if (needzoom){
                    needzoom = false;
                    mymap.fitBounds(datalayer.getBounds())
                }
                })
            }

            mymap.on('click', function(e){
                $.get('/?action=add&lon='+e.latlng.lng+'&lat='+e.latlng.lat,()=>getData());
            })

            getData()
        </script>
        </body>
        </html>
        EOF
        ```
{% endif %}

2. Pour respecter le principe d'un seul processus par conteneur, il faut lancer un second conteneur pour notre base de données. Utilisez l'image `mariadb`. Vous trouverez des informations sur la configuration de cette image sur [hub.docker.com](https://hub.docker.com/_/mariadb). Configurez la base de données de façon à ce que le code php fonctionne (nom d'utilisateur, mot de passe et base de données).

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"
        La page de présentation de l'image `mariadb` indique les variables d'environnement à utiliser pour configurer la base de données.

        Les variables à utiliser :

        | Nom | Valeur | Explication |
        | -- | :-- | :-- |
        | `MARIADB_RANDOM_ROOT_PASSWORD` | `yes` | Laisse le choix du mot de passe du super administrateur de la base à mariadb. On pourrait utiliser `MARIADB_ROOT_PASSWORD` pour choisir ce mot de passe, mais dans le contexte de ce TP, ça n'a pas d'importance. |
        | `MARIADB_DATABASE` | `mymap` | C'est le nom de la base de données à créer. Ce nom est défini dans le code PHP. |
        | `MARIADB_USER` | `user` | C'est le nom de l'utilisateur à créer. Il est défini dans le code PHP. |
        | `MARIADB_PASSWORD` | `s3cr3t` | C'est le mot de passe de l'utilisateur à créer. Il est défini dans le code PHP. |

        Le fait de définir les paramètres de connexion à la base de données dans un code source est une mauvaise pratique. Il faudrait que le code PHP détermine ces informations à partir de variable d'environnement.

        ```
        docker container run -d --name database -e MARIADB_RANDOM_ROOT_PASSWORD=yes -e MARIADB_DATABASE=mymap -e MARIADB_USER=user -e MARIADB_PASSWORD=s3cr3t  mariadb
        ```

        Ça ne fonctionne toujours pas... Le navigateur indique "getaddrinfo failed" qui indique un problème de résolution DNS, le mécanisme transformant les noms d'hôtes en IP. Si on remplace dans le fichier PHP le nom `database` par l'IP du conteneur de base de données, le site fonctionnerait, signe qu'il y a bien un lien réseau entre les conteneurs.

        Pour rappel, les conteneurs sont branchés par défaut sur le réseau `bridge` qui ne dispose pas de fonction de DNS. Il faut créer un nouveau réseau et brancher les conteneurs sur ce réseau.

        ```
        docker container stop web
        docker container rm web
        docker container stop database
        docker container rm database
        docker network create monreseau
        docker container run --net monreseau -d --name web -p 8080:80 -v $PWD/mondossier:/var/www/html/ lavoweb/php-7.1
        docker container run --net monreseau -d --name database -e MARIADB_RANDOM_ROOT_PASSWORD=yes -e MARIADB_DATABASE=mymap -e MARIADB_USER=user -e MARIADB_PASSWORD=s3cr3t  mariadb
        ```
{% endif %}

3. Avec la commande `docker container exec ...`, lancez le client mariadb en ligne de commande pour explorer la base de données et observez le contenu de la table point au fur et à mesure des interactions avec la carte.

    !!! note
        ```
        mariadb -u LOGIN -D DATABASE --password="PASSWORD"
        ```

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"

        ```shell
        $ docker container exec -it database mariadb -u user -D mymap --password="s3cr3t" 

        Reading table information for completion of table and column names
        You can turn off this feature to get a quicker startup with -A

        Welcome to the MariaDB monitor.  Commands end with ; or \g.
        Your MariaDB connection id is 17
        Server version: 10.9.3-MariaDB-1:10.9.3+maria~ubu2204 mariadb.org binary distribution

        Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

        Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

        MariaDB [mymap]> show tables;
        +-----------------+
        | Tables_in_mymap |
        +-----------------+
        | point           |
        +-----------------+
        1 row in set (0.001 sec)

        MariaDB [mymap]> select * from point;
        +----+---------+---------+
        | id | lon     | lat     |
        +----+---------+---------+
        |  1 | 3.33984 | 48.7924 |
        |  2 |  1.1206 | 46.3014 |
        |  3 | 5.00977 |  45.997 |
        |  4 | 3.54858 | 47.5246 |
        +----+---------+---------+
        4 rows in set (0.001 sec)
        ```
{% endif %}


## Base de DONNEES

1. Arrêtez et relancez le conteneur mariadb

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"

        ```shell
        docker container stop database
        docker container start database
        ```

        ou 

        ```shell
        docker container restart database
        ```
{% endif %}

2. Est-ce que les données qui avaient été saisies sont encore présentes ?
3. Oui
4. Arrêtez, supprimez et recréez le conteneur mariadb

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"
    
        ```shell
        docker container stop database
        docker container rm database
        docker container run --net monreseau -d --name database -e MARIADB_RANDOM_ROOT_PASSWORD=yes -e MARIADB_DATABASE=mymap -e MARIADB_USER=user -e MARIADB_PASSWORD=s3cr3t  mariadb
        ```
{% endif %}

5. Est-ce que les données qui avaient été saisies sont encore présentes ?
6. Non
7. Modifier la commande docker de mariadb pour placer un volume au bon endroit et retentez la suppression et recréation du conteneur mariadb.

{% if config.extra.show_correction.engine.tplamp %}
    ??? note "Correction"
    
        ```shell
        docker container stop database
        docker container rm database
        docker container run -v databasedata:/var/lib/mysql --net monreseau -d --name database -e MARIADB_RANDOM_ROOT_PASSWORD=yes -e MARIADB_DATABASE=mymap -e MARIADB_USER=user -e MARIADB_PASSWORD=s3cr3t  mariadb
        ```

        On peut trouver l'emplacement où mariadb stocke ses données avec la commande `docker image inspect mariadb` ou sur [hub.docker.com](https://hub.docker.com/_/mariadb). Les données seront stockées dans le volume docker `databasedata`. On voit ce volume dans la liste `docker volume ls`.
{% endif %}