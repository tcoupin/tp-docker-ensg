Ce support est fourni dans le cadre du cours d'architecture des systèmes d'informations, partie Docker, donné à l'ENSG par Thibault Coupin.

Vous trouverez ici les liens vers les présentations, les documents pour les travaux pratiques et des liens vers quelques exemples.

Partie 1 : TSI-[CG],  17 et 18 octobre 2024

1. [Architecture des systèmes d'informations](asi) (1/4 journée)
    - Rappel sur les SI
    - Concepts de base
    - Architecture distribuée : répartition des ressources, haute disponibilité
    - Architecture orientée service
    - Architecture cloud
2. [Prise en main de Docker Engine](docker-engine) (1 journée)
    - Présentation
    - Concepts : image, conteneur, réseau, volume
    - Dockerfile
3. [Le plugin docker compose](docker-compose) (3/4 journée)
    - Présentation
    - Le docker-compose.yml
    - Les commandes

Partie 2 : TSI-C, 10-14 février 2025

4. [Docker en cluster avec le mode swarm ](docker-swarm) (1/2 journée)
    - Concepts
    - Noeuds
    - Service
    - Stack
5. [Le projet](projet-swarm) (4 jours)
6. [Intro kubernetes](k8s) (1/2 journée)
