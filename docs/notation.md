# Grille de notation

## Tous, partie ASI, docker, compose

- QCM /20

## Groupe TSI-C, partie projet

Groupe /14

- Organisation projet /2
- Argumentaire et qualité de l'architecture /2
- 4 jalons respectés /4
- Déploiement fonctionnel /2
- Tests/résultats /4

Individuelle /6

- Intérêt/réactivité/participation /4
- Ingéniosité /2

		