# Le projet

## Sujet

**Vous incarnez une startup qui veut lancer un service de stockage en ligne à la "Google drive"**.

Le service doit être de qualité : 

- toujours accessible
- sans perte de données

C'est une idée "originale", il va y avoir beaucoup de clients et il faut :

- être en capacité d'accueillir les nouveaux clients (mise à l'échelle de l'infrastructure)
- connaître la rentabilité du service

## Contraintes

- Vous avez une semaine !
- Mise en place d'un cluster swarm
    - Clustering réseau et stockage (GlusterFS)
    - Monitoring (Prometheus)
- Déploiement d'une application de de stockage en ligne (Nextcloud) et de l'outil d'édition en ligne (OnlyOffice) + BDD (MariaDB ou PostgreSQL)
    - Configuration 
    - Tests

Les cibles à atteindre :

- haute disponibilité de l'application déployée, à tous les niveaux de l'architecture
- répartition sur l'ensemble des noeuds
- scalabilité
  
Les notions suivantes seront aussi à utiliser :

- gestion de projet
- communication

## Jalons


| Date                    | Contenu                                                                                       |
|-------------------------|-----------------------------------------------------------------------------------------------|
| mardi soir              | cluster fonctionnel (swarm + GFS), schéma d'architecture applicatif                           |
| jeudi midi              | application déployée, scalable et disponible (hors tests)                                     |
| jeudi soir              | Tests réalisés, procédures d'exploitation, abaque de rentabilité                              |
| vendredi fin de matinée | présentation de 20-30 min : but du projet, choix d'architecture, organisation, tests réalisés |

## Rendu

Un dépôt git contenant :

- script et config pour un redéploiement
- votre présentation : architecture matérielle, et l'argumentaire qui va avec pour justifier l'ensemble des choix
